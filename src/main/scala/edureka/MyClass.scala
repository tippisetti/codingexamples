package edureka

class MyClass (val name:String, val age:Int){

  // auxillary constructor
  def this(name:String){
    this(name,100) // primary constructor
  }

  def test():Unit = {
    println("Name:" + name + " " + "Age: " + age)
  }

}

object TestMyClass{
  def main(args: Array[String]): Unit = {
    val obj = new MyClass("Test",18)
    obj.test()

    val obj1 = new MyClass("test")
    obj1.test()
  }
}
