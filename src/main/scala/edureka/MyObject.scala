package edureka

object MyObject {

  def test(): Unit ={
    println("test message")
  }

  def main(args: Array[String]): Unit = {
    MyObject.test()
  }

}
