package edureka

class CompanionObjectEx2 {
// track how many instances getting created
  CompanionObjectEx2.instanceCount += 1
}

object CompanionObjectEx2{

  var instanceCount:Int = 0

  def printInstanceCount(): Unit ={
    println(instanceCount)
  }

}

object Main
{
  def main(args: Array[String]): Unit = {
    for(i<- 1 to 10)
      new CompanionObjectEx2
    CompanionObjectEx2.printInstanceCount()
  }
}