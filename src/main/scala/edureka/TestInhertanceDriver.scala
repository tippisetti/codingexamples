package edureka

abstract class MyAbstractFile{
  def open(fileName:String):Unit
  def save(fileName:String):Unit
}

class MyFile extends MyAbstractFile{
  override def open(fileName: String): Unit = {
    println("myFile.open method called")
  }

  override def save(fileName: String): Unit = {
    println("myFile.save method called")
  }
}

class MyCompressedFile extends MyFile {
  override def save(fileName: String): Unit = {
    println("MyCompressedFile.Save method called")
    println(">>> implementing compression logic")
    println("calling the immediate base save method now")
    super.save(fileName)
  }
}

  object TestInhertanceDriver {

    def main(args: Array[String]): Unit = {
      var f:MyAbstractFile = new MyFile()

      println(">>>>>>Testing My File")

      f.open(null)
      f.save(null)

      println(">>>>>>>TestingMyCompressedFile")

      f = new MyCompressedFile()

      f.open(null)
      f.save(null)
    }

}
