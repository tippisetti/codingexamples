package edureka

object functionex {

  def main(args: Array[String]): Unit = {

    // Multiply each element with 2
    val l = (1 to 20).toList
    println(l.map(n => n*2))
    println(l.map(_ * 3))

    // Convert the capitol to small

    val str = "TEST"
    println(str.map(str => str.toLower))

    // filter out only odd numbers from list

    println("odd numbers from list:- " + l.filter(n=> n%2 !=0))

    // reduce -> accumlates

    val list = List(1,2,3,4,5)
    println("reduce:- " + list.reduce((acc,n) => acc + n))

  }



}
