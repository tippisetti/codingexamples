package edureka

trait Speedometer {
  protected var speed:Float
  def showSpeed:Float
  def accelerate(rate: Float)
}

class Dashboard(var speed:Float) extends Speedometer{
  def showSpeed: Float = speed

  def accelerate(rate: Float): Unit =
  {
    println(f"Accelerating at $rate")
  }
}

object TestSpeedometer{
  def main(args: Array[String]): Unit = {
    val obj:Dashboard = new Dashboard(10)
    println("Initial Speed= " + obj.showSpeed)
    obj.accelerate(10)
  }
}
