package edureka

class CompanionObjectEx {
  def test(): Unit ={CompanionObjectEx.printMsg()
  }
}

object CompanionObjectEx{
  def printMsg(): Unit ={
    println("Test Message")
  }

  def main(args: Array[String]): Unit = {
    val obj = new CompanionObjectEx
    obj.test()
  }
}
