package edureka

// by default immutable.
// equals,copy,hashcode,toString.
case class MyCaseClass(empId: Int, empName:String, empAdd:String)

object Test{
  def main(args: Array[String]): Unit = {
    val m1 = new MyCaseClass(100, "Test1" , "Beaverton")
    val m2 = new MyCaseClass(101,"Test2","Portland")
    val m3 = m1.copy()
    println(m1==m2)
    println(m1==m3)
    println(m1) // toString already implemented in case classes
    println(m1.hashCode())
  }
}