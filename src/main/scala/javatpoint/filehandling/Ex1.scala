package javatpoint.filehandling

import java.io.{File, PrintWriter}

import scala.io.Source

class Ex1 {
  val fileObject = new File("ScalaFile.txt")
  val printWriter = new PrintWriter(fileObject)
  printWriter.write("Check It out.This is my Scala example")
  printWriter.close()

  val filename = "ScalaFile.txt"
  val fileSource = Source.fromFile(filename)

  println("reads by char:-")
  while (fileSource.hasNext){ // reads each character
    println(fileSource.next)
  }

  val fileSource1 = Source.fromFile(filename)

  for(line <-fileSource1.getLines){// reads each line
    println("reads by line:- " + line)
  }

  fileSource.close()
}

object TestEx1{
  def main(args: Array[String]): Unit = {
    new Ex1
  }


}
