package javatpoint.exceptions

// usage of Throws
class Ex3 {

  @throws(classOf[NumberFormatException])
  def validate(): Unit ={
    "abc."toInt
  }
}

object TestEx3{
  def main(args: Array[String]): Unit = {
    var e = new Ex3
    try{
      e.validate()
    }catch {
      case ex: NumberFormatException => println("exception handled here")
    }

    println("rest of the code executed")
  }
}
