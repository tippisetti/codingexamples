package javatpoint.exceptions

class Ex1 {
  def divide(a:Int,b:Int)={
    try {
      a / b
      var array = Array(a,b)
      array(30)
    }catch {
      case e: ArithmeticException => println(e)
      case ex: Throwable => println("found unknown exception" + ex)
    }
    finally{
      println("Always executes")
    }
    println("Rest of the code")
  }
}

object TestEx1{
  def main(args: Array[String]): Unit = {
    var test = new Ex1
    test.divide(100,10)
  }
}
