package javatpoint.exceptions

// custom exception
class Ex4 {
  @throws(classOf[InvalidAgeException])
  def validate(age:Int): Unit ={
    if (age<18)  throw new InvalidAgeException("not eligible")
    else println("eligible  ")
  }
}

class InvalidAgeException(s:String) extends Exception(s)

object TestEx4{

  def main(args: Array[String]): Unit = {
    var e = new Ex4
    try {
      e.validate(10)
    } catch{
      case e: Exception => println("Exception occured" + e)
    }
    print("rest of the code executed")
  }
}
