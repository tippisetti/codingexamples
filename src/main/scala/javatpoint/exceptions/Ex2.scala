package javatpoint.exceptions

// usage of Throw
class Ex2 {

  def printError: Unit ={
    println("it's your own custom error")
  }

  def validate(age:Int): Unit ={
    if(age<18) throw new ArithmeticException("you are not eligible")
    else println("You are eligible")
  }
}
object TestEx2{
  def main(args: Array[String]): Unit = {
    var e = new Ex2
    e.validate(6)
  }
}
