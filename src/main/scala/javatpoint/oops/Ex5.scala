package javatpoint.oops

object Ex5{
  def main(args: Array[String]): Unit = {
    Ex5a.hello() // no need to create a object for singletons.
  }
}

object Ex5a{
  def hello(): Unit ={
    println("Hello this is singleton object")
  }
}