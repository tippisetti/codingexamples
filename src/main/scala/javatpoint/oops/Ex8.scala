package javatpoint.oops

object Ex8 {
  // case class & pattern matching example

  def main(args: Array[String]){

    callCase(CaseClass1(10,20))
    callCase(CaseClass2(20))
    callCase(CaseObject)

  }

  def callCase(f:superTrait) = f match {
    case CaseClass1(f,g) => println(f + g)
    case CaseClass2(f) => println(f)
    case CaseObject => println("no arguments")
  }

}

trait superTrait
case class CaseClass1(a:Int,b:Int) extends superTrait
case class CaseClass2(a:Int) extends superTrait
case object CaseObject extends superTrait // case class which doesn't have arg should define as case object and it;s serializable by default.
