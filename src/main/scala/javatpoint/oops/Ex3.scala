package javatpoint.oops

object Ex3 {

  def main(args: Array[String]): Unit = {
    val s1 = new Student3(1,"foo1")
    val s2 = new Student3(2,"foo2")
    val s3 = new Student3(3,"foo3")
    val s4 = new Student3(4,"foo4")
    s1.getRecords()
    s2.getRecords()
    s3.getRecords()
    s4.getRecords()
  }
}

class Student3(id:Int,name:String){ // Primary constructor
  def getRecords(): Unit ={
    println(id +":--------:" + name)
  }
}
