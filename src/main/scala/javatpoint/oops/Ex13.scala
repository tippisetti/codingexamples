package javatpoint.oops

object Ex13 {
  def main(args: Array[String]): Unit = {
    new Programmer().showDetails()
  }
}

class EmployeeData{
  var salary:Float = 10000
}

class Programmer extends EmployeeData{
  var bonus:Int = 5000

  def showDetails(){
    println("emp salary:- " + salary)
    println("emp bonus:- " + bonus)
  }
}
