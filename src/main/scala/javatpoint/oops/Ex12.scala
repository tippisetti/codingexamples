package javatpoint.oops

object Ex12 {

  def main(args: Array[String]): Unit = {
    new ThisExample().showDetails()
    new ThisExample(100,"myName").showDetails()
  }
}

class ThisExample {
  var id:Int = 0
  var name:String = null

  def this(id:Int,name:String){
    this()
    this.id = id
    this.name = name
  }

  def showDetails(){
    println(id + " " + name)
  }
}
