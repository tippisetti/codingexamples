package javatpoint.oops

case class Ex7(a:Int,b:Int )
object TestEx7{
  def main(args: Array[String]) {

    var c = Ex7(10,10) // no need to have new keyword for init.
    println("a value:- " + c.a + "\n" +"b value:- " +c.b)
  }
}