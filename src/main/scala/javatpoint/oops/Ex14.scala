package javatpoint.oops

object Ex14 {
  def main(args: Array[String]) {
    new Vehicle().run()
    new Bike().run()
  }
}

class Vehicle{
  def run(){
    println("vehicle is running")
  }
}

class Bike extends Vehicle{
  override def run() {
    println("Bike is running")
  }
}
