package javatpoint.oops

object Ex15 {
  def main(args: Array[String]): Unit = {
    println("ICICI Bank Loan interest:- " + new Icici().getRateOfInterest()+"%")
    println("SBI Bank Loan interest:- " + new Sbi().getRateOfInterest()+"%")
    println("HDFC Bank Loan interest:- " + new Hdfc().getRateOfInterest()+"%")
  }
}
class Bank{
  def getRateOfInterest()={
    0
  }
}
class Icici extends Bank{
  override def getRateOfInterest()={
    5
  }
}
class Sbi extends Bank{
  override def getRateOfInterest()={
    6
  }
}
class Hdfc extends Bank{
  override def getRateOfInterest()={
    7
  }
}
