package javatpoint.oops

object Ex11 {
  def main(args: Array[String]): Unit = {
    var obj = new AdditionOne
    obj.add(10,20)
    obj.add(10,20,30) // method overloading
    obj.add(10.5,20.5,30.5) // method overloading diff tata type
  }
}
class AdditionOne{
  def add(a:Int,b:Int)  {
    val sum =  a + b
    println(sum)
  }

  def add(a:Int,b:Int,c:Int) {
    val sum = a+b+c
    println(sum)
  }

  def add(a:Double,b:Double,c:Double){
    val sum = a+ b + c
    println(sum)
  }
}
