package javatpoint.oops

object Ex16 {
  def main(args: Array[String]): Unit = {
    println(new BikeOne().speed)
    println(new BikeOne().gear)
  }
}

class VehicleOne{
  val speed:Int = 100
  var gear:Int = 1
  final val bell:String = "None"
}

class BikeOne extends VehicleOne {
  // val speed:Int = 120 // speed needs override modifier
  override val speed: Int = 120 // override is mandatory

  // override var gear:Int = 2 // we cannot override a mutable variable.

  // override val bell:String = "king" // final member cannot override & final method too same.
}
