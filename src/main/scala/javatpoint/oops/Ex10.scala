package javatpoint.oops

object Ex10 {

  def main(args: Array[String]) {
    new Employee1 // with default constructor
    new Employee2(100,"Emp2").showDetails() // with primary constructor
    new Employee3(101,"Emp3One").showDetails() // with auxiliary constructor
    new Employee3(102,"Emp3Two",20).showDetails()
    new Employee3(102,"Emp3Three",20,546578986).showDetails()
  }
}

class Employee1(){
  println("default constructor")
}

class Employee2(id:Int,name:String){
  def showDetails(){
    println(id + " " + name)
  }
}

class Employee3(id:Int,name:String){
  var age:Int = 0
  var num:Int = 0
  def showDetails(): Unit ={
    println(id + " " + name + " " + age + " " + num)
  }

  def this(id:Int,name:String,age:Int){
    this(id,name)
    this.age=age
  }

  def this(id: Int, name:String,age:Int, num: Int){
    this(id,name,age)
    this.num=num
  }
}
