package javatpoint.oops

class Ex6 {
  def hello(){
    println("hello this is companion class")
  }
}

object Ex6{
  def main(args: Array[String]) {
    new Ex6().hello()
    println("hello this is companion object")
  }
}
