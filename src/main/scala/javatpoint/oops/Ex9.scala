package javatpoint.oops

object Ex9 {

  def main(args: Array[String]) {
    calculate(Additions(10,20))
    calculate(Subtraction(10,20))
    calculate(Multiplication(10,20))
  }

  def calculate(a: Calculator)= a match {
    case Additions(f,g) => println("Addition of two numbers:- " +(f+g))
    case Subtraction(f,g) => println("Subtraction of two numbers:- " + (f-g))
    case Multiplication(f,g) => println("Multiplication of two numbers:- " + (f * g))
    case _ => println(" Nothing to return")
  }
}

trait Calculator
case class Additions(a:Int, b:Int)  extends Calculator
case class Subtraction(a:Int,b:Int) extends Calculator
case class Multiplication(a:Int,b: Int) extends Calculator
