package javatpoint.strings

class Ex4 {
  var s = "print this line"
  var version = 2.12
  var s1 = "Scala string example"
  var s2 = "Scala \tstring \nexample"
  var s3 = raw"Scala \tstring \nexample"
  def show(){
    println(s"printing using string interpolation s method:- $s $version")
    println(f"This is $s1%s, scala version is $version%2.2f")
    println("printing s2:-" + s2)
    println("printing s3:- " + s3)
  }
}

object TestEx4{
  def main(args: Array[String]) {
    var obj = new Ex4()
    obj.show()

  }
}
