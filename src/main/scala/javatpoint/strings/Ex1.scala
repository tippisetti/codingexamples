package javatpoint.strings

object Ex1 {
  def main(args: Array[String]): Unit = {
    var obj = new Ex1Str
    obj.show()
  }
}

class Ex1Str{
  var s1 = "Scala string example"
  def show(){
    println(s1)
    println(s1.length)
    println("head:-" + s1.head +" Tail:-" + s1.tail)
  }
}
