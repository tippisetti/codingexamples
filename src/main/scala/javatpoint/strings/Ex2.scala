package javatpoint.strings

object Ex2 {
  def main(args: Array[String]): Unit = {
    var obj = new Ex2Test()
    obj.show()
  }
}

class Ex2Test{
  var s = "string example"
  // "modification of string" + s //
  s = "modification of string" + s // s variable now refers to new string object.
  def show(){print(s)}
}
