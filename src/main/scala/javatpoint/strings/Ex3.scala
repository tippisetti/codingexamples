package javatpoint.strings

class Ex3 {
  var s1 = "scala is best"
  var s2 = "coding example"
  var s3 = "coding example"
  var s4 = "coding example" + "of scala"
  var s5 = "learn it"

  def show(){
    println(s1==s2) // == compares the reference
    println(s2==s3)
    println(s1.equals(s2)) // equals checks the actual content
    println(s2.equals(s3))
    println(s1.compareTo(s2))// compareTo returns (s1>s2) +ve , (s1<s2) _ve or (s1==s2) zero
    println(s2.compareTo(s3))
    println(s4.concat(s4)) // usage of concat method, check s4 it's using + operator.
    println(s4.substring(14 ,22))// usage of substring
  }
}

object TestEx3{
  def main(args: Array[String]): Unit = {
    var obj = new Ex3
    obj.show()
  }
}
