package javatpoint.traitEx

object Ex6 {
  def main(args: Array[String]): Unit = {
    var obj = new PrintableClass5 with Printable5
    obj.print()
  }
}

trait Printable5{
  def print()
}

class PrintableClass5{
  def print(){println("This is printable")}
}
