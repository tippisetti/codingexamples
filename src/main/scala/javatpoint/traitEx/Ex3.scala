package javatpoint.traitEx

object Ex3 {
  def main(args: Array[String]): Unit = {
    var obj = new A6()
      obj.print()
      obj.show()
  }
}

trait Printable2{
  def print()
}

trait Showable2{
  def show()
}

class A6 extends Printable2 with Showable2{
  def print(){
    println("This is printable / trait override method")
  }

  def show(){
    println("This is showable/trait override method")
  }
}