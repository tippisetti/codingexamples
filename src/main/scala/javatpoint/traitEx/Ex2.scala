package javatpoint.traitEx

object Ex2 {
  def main(args: Array[String]): Unit = {
    var obj = new A412
    obj.print()
    obj.printA4()
  }
}

trait Printable1{
  def print()
}

abstract class A41 extends Printable1{ // if we don't override print then it has to be abstract
  def printA4(): Unit ={
    println("A4 abstract printer")
  }
}

class A412 extends A41{
  def print(){println("this is A412 class print/trait override")}
}
