package javatpoint.traitEx

object Ex1 {
  def main(args: Array[String]){
    var obj = new A4
    obj.print()
  }
}

trait Printable{
  def print()
}

class A4 extends Printable{
  def print(){
    println("Hello")
  }
}