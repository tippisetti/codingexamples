package javatpoint.traitEx

object Ex5 {
  def main(args: Array[String]): Unit = {
    var obj = new PrintShow
    obj.print()
    obj.show()
  }
}

trait Printable4{
  def print()
}

abstract class Showable4{
  def show()
}

//class PrintShow extends Printable4 with Showable4 { // this line throws error because traits must be extended after this class or abstract class only(mixin order).)

class PrintShow extends Showable4 with Printable4 {
  def print(){println("This is printable")}
  def show(){println("this is showable")}
}
