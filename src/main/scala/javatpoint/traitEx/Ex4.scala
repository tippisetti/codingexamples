package javatpoint.traitEx

object Ex4 {
  def main(args: Array[String]): Unit = {
    var obj = new A8()
    obj.print()
    obj.show()
  }
}

trait Printable3{
  def print()
  def show(){ println("This is showable method in trait")}
}

class A8 extends Printable3{
  def print(){println("overriding trait method in A8")}
}
