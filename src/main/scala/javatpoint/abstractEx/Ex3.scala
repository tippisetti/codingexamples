package javatpoint.abstractEx

import example.Greeting

object Ex3 {

  def main(args: Array[String]) {
    val dog = new Dog("Fido")
    dog.sayHello()
    println(dog)
    val cat = new Cat("Morris")
    cat.sayHello()
    println(cat)
  }

}

abstract class Pet(name:String){
  val greeting:String
  var age:Int
  def sayHello(){println(greeting )}
  override def toString: String = s"I say $greeting, and I'm $age"
}

class Dog(name:String) extends Pet(name){
  val greeting = "woof"
  var age = 20
}

class Cat(name:String) extends Pet(name){
  val greeting = "Meow"
  var age = 16
}
