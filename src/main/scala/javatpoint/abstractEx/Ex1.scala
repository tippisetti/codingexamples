package javatpoint.abstractEx

object Ex1Bike {
  def main(args: Array[String]) {
    new TestBike().run()
  }
}

abstract class Bike{
  def run()
}

class TestBike extends Bike{

  def run(){
    println("TestBike extended abstract method of bike")
  }

}
