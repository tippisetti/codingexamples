package javatpoint.abstractEx

class Ex2(a:Int) extends Bike1(a) {
  c =30
  def run(): Unit ={
    println("running fine ")
    println("a:- " + a + " b:- " + b +" c:- " + c)
  }
}

abstract class Bike1(a:Int){
    var b:Int = 20
    var c:Int = 25
    def run() // abstract method
    def performance(){
      println("performance method-non abstract")
    }
}

object TestEx2{
  def main(args: Array[String]): Unit = {
    val obj = new Ex2(10)
    obj.run()
    obj.performance()

  }
}
