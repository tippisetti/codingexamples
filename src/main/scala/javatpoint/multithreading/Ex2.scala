package javatpoint.multithreading

class Ex2 extends Thread{
  override def run(){
    for(i<-0 to 20){
      println(this.getName + "--->" + i)
      Thread.sleep(300)
    }
  }
}

object TestEx2{
  def main(args: Array[String]){
    val obj1 = new Ex2()
    obj1.start()
    obj1.join() // join() method is used to hold the execution of currently running thread
    obj1.setName("Thread1")
    val obj2 = new Ex2()
    obj2.start()
  }
}
