package javatpoint.multithreading

class Ex1 extends Thread{
  override def run(){
    println("thread is running (Thread)")
  }
}

class Ex1a extends Runnable{
  override def run(): Unit ={
    println("thread is running (Runnable)")
  }
}

object TestEx1{
  def main(args: Array[String]): Unit = {
    var obj1 = new Ex1
    obj1.start()
    println("thread name:- " + obj1.getName)

    var obj2 = new Ex1a
    var obj2a = new Thread(obj2)
    obj2a.start()
    println("thread name:-" + obj2a.getName)

  }

}
