package javatpoint.tuple

object Ex2 {

  // Returning multiple values using tuple
  // usage of productIterator

  def main(args: Array[String]): Unit = {

    var tuple = tupleFunction()
    println("Iterating values ")
    tuple.productIterator.foreach(println)
  }

  def tupleFunction() = {
    var tuple = (1,4,5,4.5,"foo1")
    tuple
  }
}
