package javatpoint.tuple

object Ex1 {
  def main(args: Array[String]): Unit = {

    // a tuple groups together simple logical collections of items without using a class.
    var hostPort = ("localHost",80)
    println(hostPort._1)
    println(hostPort._2)

    // tuple of integer
    var tuple = (1,2,3,5)
    var tuple1 = ("foo1","foo2","foo3")
    var tuple2 = (1.5,4.5,10.5)
    var tuple3 =(1,1.5,"foo1")
    println(tuple + " " + tuple1 +" " + tuple2 + " " + tuple3)
    println("fetching first value from tuple 3:- " + tuple3._1)
    println("fetching second value from tuple 3:- " + tuple3._2)
    println("fetching last value from tuple 3:- " + tuple3._3)
  }
}
