package javatpoint.modifiers

object Ex2 {
  def main(args: Array[String]){
    var obj = new Test2Ex2()
    obj.display()
  }
}

class Test1Ex2{
  protected var a:Int = 10
}

class Test2Ex2 extends Test1Ex2{
  def display(){println(a)}
}
