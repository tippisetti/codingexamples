package javatpoint.modifiers

object Ex1 {
  def main(args: Array[String]): Unit = {
    var obj = new AccessEx
    obj.show()
//  obj.x = 20 // it will return an error and cannot be accessed.

  }
}

class AccessEx{
  private var x:Int = 10

  def show(){println(x)}
}
