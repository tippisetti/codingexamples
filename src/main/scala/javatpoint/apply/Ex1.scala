package javatpoint.apply

object Ex1 {
  def apply(name:String):String = {
    "Hello %s".format(name)
  }

  def main(args: Array[String]): Unit = {
    println("string with apply method:- " + Ex1.apply(" there"))
    println("string without apply method:- " + Ex1(" there"))
  }
}
