package javatpoint.apply

object Ex4 {
  def main(args: Array[String]): Unit = {
    val func = (x:String) => "hello %s".format(x)

    println(func("world"))
    println(func.apply("world"))
  }
}
