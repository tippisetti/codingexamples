package javatpoint.apply

object Ex5 {
  def main(args: Array[String]): Unit = {
    val computer = Computer("MyComputer")
    println(goIdle(computer))

    val phone = Phone("MyPhone")
    println(goIdle(phone))
  }

  abstract class Device
  case class Computer(model:String) extends Device{
    def screenSaverOn =   "turning screen saver on"
  }
  case class Phone(model:String) extends Device{
    def screenOff ="turning screen off"
  }

  def goIdle(device: Device) = device match {
    case p:Phone => p.screenOff
    case c:Computer => c.screenSaverOn
  }
}
