package javatpoint.apply

object Ex3 {

  class Amazing{
    def apply(x:String) =   "Amzing %s!"   .format(x)
  }

  def main(args: Array[String]): Unit = {
    val amazing = new Amazing()
    println(amazing("World"))
  }
}
