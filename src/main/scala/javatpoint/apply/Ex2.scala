package javatpoint.apply

object Ex2 {

  // Case classes provide you with an automatically generated apply function

  case class Person(name:String,age:Integer,favColor:String)

  def main(args: Array[String]): Unit = {
    val p1 = new Person("person1",28,"blue")
    val p2 = Person("person2",30,"green")
    val p3 = Person.apply("person3",40,"red")
    val p4 = List(p1,p2,p3)
    println(p4)
  }
}
