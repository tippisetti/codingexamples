package javatpoint.patternmatching

// deconsturcting objects
// type checking
// test if conditions and taking actions

class Ex2 (p_id:String,p_msg:String){
  val id = p_id
  val msg = p_msg

  def testMessagePat(id:String,msg:String):String = msg match {
    case id => "return Id:-" + id
    case msg => "return msg" + msg
  }
}

object TestEx2{
  def main(args: Array[String]): Unit = {
    var result = new Ex2("100","Test Message")
    var result1 = result.testMessagePat("100","Test Message1")
    println(result1)

  }
}
