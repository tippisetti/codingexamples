package javatpoint.patternmatching

object Ex1 {

  // patternMatching - matching object againist other objects
  // typed pattern match - does it match string,int or something else

  def myTest(x:Any) = x match {
    case i:Integer => "It's an integer " + i
    case j:String => "It's a string" + j
    case d:Double => "It's a double " + d
    case _ => "Something else"
  }

  def main(args: Array[String]): Unit = {
    println(myTest(100))
  }
}
