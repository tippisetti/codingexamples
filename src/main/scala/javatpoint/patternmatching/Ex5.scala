package javatpoint.patternmatching

object Ex5 {

  abstract class Notification
  case class Email(sender:String,title:String,body:String) extends Notification
  case class SMS(caller:String,message:String) extends Notification
  case class VoiceRecording(contactName:String,link:String) extends Notification


  def showNotification(notification: Notification):String = {
    notification match {
      case Email(email,title,_) => s"You got an email from $email with $title"
      case SMS(number,message) => s"you got an sms from $number ! message: $message"
      case VoiceRecording(cn,link) =>  s"you received a voice recording from $cn. Click the link to hear $link"
    }
  }

  def main(args: Array[String]): Unit = {

    val email = Email("email@gamil.com","title","hi there")
    println(showNotification(email))
    val someSMS = SMS("100","Hello there")
    println(showNotification(someSMS))
    val vr = VoiceRecording("782434","link.net")
    println(showNotification(vr))
  }

}
