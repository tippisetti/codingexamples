package javatpoint.patternmatching

object Ex4 {

  def main(args: Array[String]): Unit = {

    val alice =  Person("Alice",35)
    val bob =  Person("Bob",30)
    val charlie = Person("Charlie",20)

    for (person <- List(alice,bob,charlie)){
      person match{
        case Person("Alice",35) => println("Hi Alice")
        case Person("Bob",30) => println("Hi Bob")
        case Person(name,age) => println(age +" " + name)
      }
    }

  }

  case class Person(name:String,age:Int)

}
