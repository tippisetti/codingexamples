package javatpoint.patternmatching

case class Ex3Case(ph:String,extn:Int) {
}

object TestEx3Case{
  def main(args: Array[String]): Unit = {
    val extension = List(Ex3Case("test1",100),Ex3Case("test2",200))
    val result = extension.filter{
      case Ex3Case(ph,extn) => extn < 200
    }
    println(result)
  }

}
