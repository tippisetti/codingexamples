package javatpoint.functions

object Ex2 {

  var factor =3
  val multiplier = (i:Int) => i * factor

  def main(args: Array[String]): Unit = {
    println("multiplier of 3:- " + multiplier(3))
    println("multiplier of 5:- " + multiplier(5))
  }

}
