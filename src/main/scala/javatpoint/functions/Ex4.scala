package javatpoint.functions

object Ex4 {

  def main(args: Array[String]): Unit = {

    // Passing a Function as Parameter in a Function
    functionExample(25,multiplyBy2)

    // Function Composition
    println(multiplyBy2(add2(10)))

    // Anonymus function

    val result1 = (a:Int,b:Int) => a+b // Anonymous function by using => (rocket)
    println(result1(10,20))

    val result2 = (_:Int) + (_:Int)  // Anonymous function by using _ (underscore) wild card
    println(result2)

    // Scala Function Currying Example

    println(add(10)(20))

  }

  def add(a:Int)(b:Int) = {
    a + b
  }

  def functionExample(a:Int, f:Int => AnyVal): Unit ={
    println(f(a))
  }

  def multiplyBy2(a:Int):Int={
    a * 2
  }

  def add2(a:Int):Int = {
    a + a
  }

}
