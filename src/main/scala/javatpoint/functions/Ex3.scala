package javatpoint.functions

object Ex3 {

  def main(args: Array[String]): Unit = {
    //Scala Function Example without using = Operator
    print("Function Example without using = Operator:-")
    functionExample1()

    //Scala Function Example with using = Operator
    print("Function Example with using = Operator:-")
    println(functionExample2())

    //Scala Parameterized Function Example
    print("Parameterized Function Example:-")
    println(functionExample3(10,20))

    //Scala Recursion Function
    print("Recursion function:- ")
    println(functionExample4(15,2))

    // Function Parameter example with default value
    print("Function Parameter example with default value:- ")
    println(functionExample5())
    println("without default value" + functionExample5(10,20))

    //Scala Function Named Parameter Example
    print("Function Named Parameter Example:- ")
    println(functionExample6(b=10,a=5))

  }

  def functionExample1() {
    println("this is a simple function")
  }

  def functionExample2()= {
    var a = 10
    a
  }

  def functionExample3(a:Int,b:Int):Int = {
    a + b
  }

  def functionExample4(a:Int,b:Int):Int = {
    if(b==0)
    0
    else
      a+functionExample4(a,b-1)
  }

  def functionExample5(a:Int = 0,b:Int =0): Int ={
    a + b
  }

  def functionExample6(a:Int,b:Int):Int = {
    a + b
  }

}
