package javatpoint.functions

object Ex1 {
  var sum:Int = 0
  def addInt(a:Int,b:Int):Int = {
    sum = a + b
    sum
  }

  def printMe()={
    print(sum)
  }

  def main(args: Array[String]): Unit = {
    addInt(10,20)
    println(sum)
    println("Return value " + addInt(30,40))
  }

}
