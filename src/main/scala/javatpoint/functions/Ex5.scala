package javatpoint.functions

object Ex5 {

  def sumInts(a:Int, b:Int):Int = if (a>b) 0 else a + sumInts(a+1,b)
  def main(args: Array[String]): Unit = {

    println(sumInts(1,11))

  }

}
