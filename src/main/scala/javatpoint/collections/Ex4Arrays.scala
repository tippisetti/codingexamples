package javatpoint.collections

object Ex4Arrays {
  def main(args: Array[String]): Unit = {

    // Array Example
    var arrayEx = Array(2,3,1,44,5)
    println("Length of ArrayEx:- " + arrayEx.length)

    //Access show method and print array
    var arrPrint = new Ex4ArrayExample
    arrPrint.show()

    // array with new keyword which initialize memory for array

    var arrE1 = new Ex4ArrayExample
    arrE1.show1

    // array with foreach loop

    var arrEx3 = Array(1,2,3,5,6)
    arrEx3.foreach((e:Int) => println(e))
  }

  // passing array as an argument to a function

  var arr = Array(341,332,23,24,65,76)
  var arrEx4 = new Ex4ArrayExample
    arrEx4.show2(arr)
}

class Ex4ArrayExample{
  var arr = Array(1,2,3,4,5)
  var arrE1 = new Array[Int](5)
  def show() {
    for(a<-arr){println(a)}
    println("third element of array before assignment:-" + arr(2))
    arr(2) = 10
    println("third element of array after assignment:-" + arr(2))

  }

  def show1() {
    for(a<-arrE1){println(a)}
    println("third element of array before assignment:-" + arrE1(2))
    arrE1(2) = 10
    println("third element of array after assignment:-" + arrE1(2))
  }

  def show2(arr:Array[Int]) {
    for(a<-arr){
      println(a)
    }
  }
}
