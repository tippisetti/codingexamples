package javatpoint.collections

object Ex6recursion {
  def main(args: Array[String]): Unit = {
    val list = buildList()
    println(list)
    println(concatString(list))
  }

  def buildList():List[String] = {
    val input = readLine()
    if(input=="quit" ) Nil
    else input :: buildList()
  }

  // concat strings
  def concatString(words: List[String]):String = {
    if(words.isEmpty) ""
    else words.head + concatString(words.tail)
  }
}
