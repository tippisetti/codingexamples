package javatpoint.collections

import scala.collection.immutable.{HashSet, ListSet}
import scala.collection.{BitSet, SortedSet, mutable}

object Ex1Set {

  def main(args: Array[String]): Unit = {

    // print list
    val set1 = Set()
    val games = Set("Cricket","Tennis","Golf","Table Tennis","Tennis")
    println(set1)
    println(games)

    // filter the game which starts with T
    val filterGames = games.filter(g=>g.startsWith("T"))
    println("games which starts with T:- " + filterGames)

    // return first element of game
    println("Returns first element of game:- " + games.head)

    // return all elements except first one
    println("Returns all except first one:- " + games.tail)

    // merge two sets
    val games1 = Set("Badminton")
    var mergeSet = games ++ games1
    println("merge two sets:- " + mergeSet)

    // return size of the list
    println("Return size of the list:- " + mergeSet.size)

    // Check element is present or not
    println("check element :- " + mergeSet.contains("FootBall"))

    // add and remove elements from a set
    mergeSet += "Chess"
    println("adding element:- " + mergeSet)
    mergeSet -= "Chess"
    println("removed the element:- " + mergeSet)

    // Iterate set using for loop
    println("mergeSetPrint loop start")
    for (mergeSetPrint <- mergeSet){
      println( mergeSetPrint)
    }
    println("mergeSetPrint loop end")

    // Iterate set using for each
    println("mergeSetPrint loop start using foreach")
    mergeSet.foreach((e:String) => println(e))

    // Sorting of set

    var sortMergeSet = SortedSet("aTest","cTest","bTest","fTest")
    println("Sorted Set example" + sortMergeSet)

    // HashSet - uses hash code to store elements.
    // It neither maintains insertion order nor sorts the elements
    var hashset = HashSet(4,2,8,0,6,3,45)
    hashset.foreach((element:Int) => println(element+" "))

    // BitSet example
    var numbers = BitSet(0,4,2,8,6,9)
    numbers.foreach((e:Int) => print(e + ""))
    numbers += 20
    println()
    println("After adding 20:- ")
    numbers.foreach((e:Int)=> print(e+" "))
    numbers -= 20
    println()
    println("After deleting 20:- ")
    numbers.foreach((e:Int) => print(e + " "))
    println()

    // ListSet - maintains insertion order,stores in reversed insertion order
    var listSet:ListSet[String] = new ListSet() // empty list by constructor
    var listSet1:ListSet[String] = ListSet.empty // empty list set
    println("empty list created by constructor:- " + listSet)
    println(listSet1)
    listSet += "Add to listSet"
    listSet1 += "Add to listSet1"
    println(listSet)
    println(listSet1)
  }

}
