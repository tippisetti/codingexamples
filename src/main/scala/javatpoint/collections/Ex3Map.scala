package javatpoint.collections

import scala.collection.immutable.{HashMap, ListMap}

object Ex3Map {
  def main(args: Array[String]): Unit = {

    // Map - to store elements as key value pair
    var map = Map(("A","Apple"),("C","Call"))
    println(map)
    var map1 = Map(("B","Bat"))
    var mergeMap = map ++ map1
    println("merging two maps:- " + mergeMap)
    var addElement = mergeMap+("D"->"Doctor")
    println("adding new element to mergeMap:- " + addElement)

    var removeElement = mergeMap - ("D")
    println("Removal of an element:- " + removeElement)

    // HashMap - uses hashcode to store elements and returns map

    var hashMap = new HashMap()
    var hashMap1= HashMap("A"->"Apple","B"->"Battle")
    println("printing empty hashMap:- " + hashMap)
    println("printing hashmap1:- " + hashMap1)

    // printing hashmap using for each loop
    hashMap1.foreach{
      case (key,value) => println(key + " " + value)
    }

    // printing using key value
    println("print a value using a key:- " + hashMap1("A"))

    // ListMap - maintains insertion order and returns listMap

    var listMap = ListMap("Rice" -> "100", "Gram" -> "500")
    var listMap1 = new ListMap()
    var listMap2 = ListMap.empty
    println(listMap)
    println(listMap1)
    println(listMap2)

    var newListMap = listMap +("Pulses"->"1000")
    println(newListMap)

    // Map.get uses Option for its return type.
    // Option tells you that the method might not return what you’re asking for.
    // Option- Option itself is generic and has two subclasses: Some[T] or None

    val numbers = Map("One" -> 1,"Two" -> 2)
    println("usage of Some :- " + numbers.get("Two"))
    println("usage of None :- " + numbers.get("Three"))
  }
}
