package javatpoint.collections

case class Ex7(var name:String, var age:Int)

object TestEx7{
  def main(args: Array[String]): Unit = {
    val obj = List(Ex7("test1",10),Ex7("name1",20),Ex7("foo1",40))
    val objSortedByf = obj.filter(ex=>ex.name.startsWith("f"))
    println(objSortedByf)
    val objSortedByAge20 = obj.filter(ex=>ex.age>=20)
    println(objSortedByAge20)

    for (cnt <- 1 to 25){
      var obj = new Ex7("name100",20)
      obj.age = cnt
      obj.name = "name1"+cnt

      println(obj.name + " " + obj.age)
    }
  }
}