package javatpoint.collections

import scala.collection.immutable.Queue

object Ex2Seq {

  def main(args: Array[String]): Unit = {

    // sequence- access using index & insertion order maintains.
    var seq:Seq[Int] = Seq(20,30,10,40)
    seq.foreach((e:Int)=>println(e))
    println("access by using index:- " + seq(2))

    var check40 = seq.contains(50)
    println("usage of contains:- " + check40)
    println("usage of ends with:- " + seq.endsWith(Seq(10,40)))
    println("usage of reverse method:- " + seq.reverse)

    // Vector - random access elements, good for large collections
    var vectorEx:Vector[Int] = Vector(10,30,20,5)
      var vectorEx1 = vectorEx:+ 50
    println("vectorEx data:- " + vectorEx1)
    var mergeTwoVectors = vectorEx++vectorEx1
    println("printing two merged vectors with sorting:- " + mergeTwoVectors.sorted)


    // print list
    val x1= List()
    val x = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    println("My empty list:- " + x1)
    println("print my list:- " + x)

    // filter evens
    val filterEvens = x.filter(n => n % 2 == 0)
    println("Print all Evens:- " + filterEvens)

    // map method usage
    val mapSquare = x.map(a => a * a)
    println("Square with Map:- " + mapSquare)

    // filter takeWhile
    val y = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    val z = y.takeWhile(a => a < 6)
    println("filter with takeWhile:- " + z)

    // sum of all elements in the list
    println(x.sum)

    // merging two lists
    val mergeList = x ++ y
    println("Merge of two lists:- " + mergeList)


    // Queue: allows inserting and retrieving elements in a first-in-first-out (FIFO) manner
    var queue = Queue(1,5,6,2,3,9,5,2,5)
    var queue2:Queue[Int] = Queue(1,5,6,2,3,9,5,2,5)
    println("usage of front method:- " + queue2.front)
    println("usage of enqueue :- " + queue.enqueue(100))
    println("usage of dequeue :- " + queue.dequeue)

    // Stream: is lazy list
    // evaluates elements only when they are required
    val stream1 = 100#:: 200#:: 85#:: Stream.empty
    println("printing stream one:- " + stream1)
    val stream2 = (1 to 10).toStream
    println("Printing stream two: - " + stream2)
    println("usage of head method:- " + stream1.head)
  }
}
