package javatpoint.collections
object Ex5ListObject{

case class Stud(rollNo: Int, name:String, marks:Int)

  val students = List(Stud(1,"name1",80),Stud(2,"name2",50),Stud(3,"name3",40),Stud(4,"name4",70))

  val first =students.head  // returns first element
  val rest = students.tail // returns all elements except first
  val toppers = students.filter(s=> s.marks >=60) // returns only marks >-60
  val partions = students.partition(s=>s.marks>60) // returns marks>=60 & less in two lists


  case class Teachers(name:String,subject:String)
  val teachers = List(Teachers("name1","mysubject1"),Teachers("name2","subject2"))
  val filterTeachers = teachers.filter(t=> t.subject startsWith("m"))

  def main(args: Array[String]): Unit = {
    println(toppers)
    println(partions)
    println(filterTeachers)
  }
}