package javatpoint.collections

object Ex8 {
  def main(args: Array[String]): Unit = {
    val setObj = Set(1,2,3)
    // setObj += 1 // not possible for immutable collection.
    println(setObj)
    val newSetObj = setObj+7
    println(newSetObj)
    val newSetObj1 = newSetObj.toList
    println(newSetObj1)

    val setMuteObj = scala.collection.mutable.Set(1,2,3)
    setMuteObj+=100 // possible to add for mutable collections.
    println(setMuteObj)


  }
}

