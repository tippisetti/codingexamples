package functions

object Closure {

  // Closure in scala:  function return value depends on outside the variable of a function.

  val factor = 3
  def multiplier(i:Int) = {
    println(i*factor)
  }

  def main(args: Array[String]){
    multiplier(3)
    multiplier(5)
  }
}
