package functions

object PartialFunction {

  def main(args: Array[String]){

    val pfa = (x:String, y:String) => x + y
    val email = pfa(_:String, " functions")
    println(email("partially applied"))

  }

}
