package functions

object CurryingFunction {

  //passing the arguments to a function in multiple parameters

  def add (a:Int)(b:Int) = a+b

  def main(args: Array[String]) {

    var result = add(10)(10)
    println(result)

    var addIt = add(10)_
    var result2 = addIt(12)
    println(result2)


  }

}
