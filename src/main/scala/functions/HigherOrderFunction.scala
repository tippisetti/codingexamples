package functions

object HigherOrderFunction {

  // (a function which takes function as parameter, returns function)


  def functionExample(a: Int, f:Int => AnyVal) : Unit = println(f(a))

  def multiplyBy2(a:Int): Int = a*2

  def main(args: Array[String]) {

    functionExample(25,multiplyBy2)

  }
}
