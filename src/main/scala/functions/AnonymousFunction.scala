package functions

object AnonymousFunction {

  def main(args: Array[String]) {

    //a function which doesn’t have a name.

    var result = (x:Int, y:Int) => x+y
    var result1 = (_:Int) + (_:Int)

    println(result(10,10))
    println(result1(10,10))

  }

}
