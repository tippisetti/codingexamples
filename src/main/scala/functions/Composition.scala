package functions

object Composition {

  //  (composing more than one function. Passing a function as parameter to another function)


  def add(a:Int): Int = a+2

  def multiplyBy2(a:Int) : Int = a*2

  def main(args: Array[String]){

    var result = multiplyBy2(add(10))
    println(result)

  }
}
