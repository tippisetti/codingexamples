package functions

object NestedFunction {

  def add(x:Int,y:Int,z:Int) : Int = {
    def add2(x:Int,y:Int) = {
      x+y
    }
    add2(x,add2(x,y))
  }

  def main(args: Array[String]) {
    println(add(10,10,10))
  }
}
