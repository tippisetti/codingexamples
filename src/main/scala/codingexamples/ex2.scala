package codingexamples

object ex2 {

  def main(args: Array[String]): Unit = {

    // take a list filter out odd numbers , square them, find the sum

    val list = List(1,2,3,4,5)

    println(list.filter(n => n%2 !=0).map(n => n * n).sum)

    // declaring as wild card

    println(list.filter(_ % 2 !=0).map(n=> n * n).sum)

  }
}
