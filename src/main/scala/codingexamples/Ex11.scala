package codingexamples

//scala auxilary constructor
class Ex11 (id:Int, name:String){
  var age:Int = 0
  def showDetails(): Unit ={
    println(id + " " +name + " " + age)
  }

  def this(id:Int,name:String,age:Int){
    this(id,name)
    this.age = age
  }
}

object TestEx11{
  def main(args: Array[String]): Unit = {

    var obj = new Ex11(100,"TestName",20)
    obj.showDetails()

    var obj1 = new Ex11(100,"TestName1")
    obj1.showDetails()
  }
}
