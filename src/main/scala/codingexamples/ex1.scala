package codingexamples

object ex1 {
  def areaRect(l:Int, b:Int):Int = l * b

  val f = areaRect _

  def add(a:Int, b:Int):Int = a + b
  def mul(a:Int,b:Int):Int = a* b

  // passing function which takes two integers add or mul etc
  def operation(f:(Int,Int) => Int, op1:Int, op2:Int) :Int = f(op1,op2)

  //
  def genericOperation[T <: Any](f:(T,T) => T, op1:T,op2:T):T = f(op1,op2)

  def main(args: Array[String]): Unit = {
    println(areaRect(10,20))
    println(f(10,20))

    val d = operation(add,10,10)
    println(d)

    println(operation(mul,10,10))

    println(operation((a,b) => a + b, 9, 7))
    println(operation((a,b) => a * b, 9, 7))
    println(operation((a,b) => a - b, 9, 7))

    println(genericOperation[Float]((x,y) => x+y , 2.5f,3.2f))
  }
}
