package codingexamples

//Constructor overloading
class Ex12(id:Int) {
  def this(id:Int, name:String){
    this(id)
    println(id + " " + name)
  }
  println(id)
}

object TestEx12{
  def main(args: Array[String]): Unit = {
    new Ex12(100)
    new Ex12(101,"TestName")
  }
}
