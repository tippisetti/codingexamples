package codingexamples

object ex4 {

  def main(args: Array[String]) {

    val x = "Hi There "
    println("index of :- " + x.indexOf("i"))
    println("substring:- " + x.substring(2))
    println("splitAt:- " + x.splitAt(2))
    println("trim:-" + x.trim.length )
    println("reverse:-" + x.reverse)
    println("concat:-" + x.concat(" Hello"))
    println("length before trim" +x.length +"trim:- " + x.trim + "length after trim" + x.length)
  }
}
