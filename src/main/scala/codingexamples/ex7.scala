package codingexamples

// trait,caseclass,caseobject,pattern matching

trait ex7
case class caseClass1(x:Int, y:Int) extends ex7
case class caseClass2(x:Int) extends ex7
case object caseObject extends ex7

object MainObject{
  def main(args: Array[String]): Unit = {

    callCase(caseClass1(10,20))
    callCase(caseClass2(10) )
    callCase(caseObject)
  }

  def callCase(f:ex7) = f match {
    case caseClass1(f,g) => println( f + g)
    case caseClass2(f) => println(f)
    case caseObject => println("no argument")
  }
}
