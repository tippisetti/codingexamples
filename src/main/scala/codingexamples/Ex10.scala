package codingexamples

//scala primary constructor
class Ex10(id:Int, name:String) {
  def showDetails(): Unit ={
    println(id + " " + name)
  }
}

object TestEx10{
  def main(args: Array[String]): Unit = {
    var s = new Ex10(100,"Test1")
    s.showDetails()
  }
}
