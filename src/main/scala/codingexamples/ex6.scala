package codingexamples

case class ex6 (x:Int, y:Int){

}

object TestEx6{
  def main(args: Array[String]): Unit = {
    var c =   ex6(10,10)
    println("x value:- " + c.x + " y value:-" + c.y)
  }
}
