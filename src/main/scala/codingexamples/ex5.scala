package codingexamples

// Companion object example
object ex5 {
  def hello(): Unit ={
    println("Hello this is companion Object")
  }

  def main(args: Array[String]): Unit = {
    hello();
    new ex5().hello()
  }
}

class ex5{
  def hello(): Unit ={
    println("Hello this is companion class")
  }
}
