package codingexamples

import scala.util.control.Breaks._
object TestEx13{
  def main(args: Array[String]): Unit = {

    breakable{
      for (i<- 1 to 20 by 2){
        if(i==7)
        break
        else
          println(i*10)
      }
    }
  }
}
