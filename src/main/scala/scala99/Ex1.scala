package scala99

object Ex1 {
  def main(args: Array[String]): Unit = {

    // P01 (*)- Find the last element of a list
    var list = List(29,39,19,23)
    var counter:Int = 0

    for(i<-list) counter+=1
    println("Last element of list is :- " + list(counter-1))

    //P02 (*) Find the last but one element of a list.

    println("Last but one of a list is:- " + list(counter-2))
  }
}
